<?php

/**
 * @author Luis E. Tineo <luis@codegrease.com>
 * @package CodeGrease
 * @subpackage CodeGrease_Shell
 * @copyright  Copyright (c) 2015   CodeGrease, LLC (http://www.codegrease.com)
 */
require_once 'abstract.php';

class CodeGrease_Shell_CatalogUpdate extends Mage_Shell_Abstract {

    /**
     * Run script
     *
     */
    public function run() {
        Mage::getModel("catalogrule/observer")->runCatalogRulesUpdate();
    }

    /**
     * Retrieve Usage Help Message
     *@todo we need to add some potential usage help 
     * like setting up debugging from here
     */
    public function usageHelp() {
        return 'Usage:  php -f catalog_update.php ';
    }

}

$shell = new CodeGrease_Shell_CatalogUpdate();
$shell->run(); 
