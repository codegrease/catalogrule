<?php

/**
 * @author Luis E. Tineo <luis@CodeGrease.com>
 * @package CodeGrease
 * @subpackage CodeGrease_CatalogRule
 * @copyright  Copyright (c) 2015   CodeGrease, LLC (http://www.codegrease.com)
 */
class CodeGrease_CatalogRule_Model_Observer extends Mage_CatalogRule_Model_Observer {

    protected $_name = "catalogrulesqueue";
    protected $_debug = false;
    protected $_log = "catalogrulesqueue.log";
    protected $_queue = null;

    const MAX_ALLOWED_MESSAGES = 100;

    /**
     * Apply all catalog price rules for specific product
     *
     * @param   Varien_Event_Observer $observer
     * @return  Mage_CatalogRule_Model_Observer
     */
    public function applyAllRulesOnProduct($observer) {
        $product = $observer->getEvent()->getProduct();
        if ($product->getIsMassupdate()) {
            return;
        }
        $data = serialize(array('pid' => $product->getId(), 'handle' => 'catalogrule/rule||applyAllRulesToProduct'));
        $this->getQueue()->send($data);

        return $this;
    }

    /**
     * Builds/gets a queue specified by $this->_name
     * 
     * @return Zend_Queue
     */
    protected function getQueue() {
        if (!Mage::registry($this->_name)) {
            $db = simplexml_load_file('app' . DS . 'etc' . DS . 'local.xml');
            $settings = $db->global->resources->default_setup->connection;
            $queueOptions = array(
                Zend_Queue::NAME => $this->_name,
                'driverOptions' => array(
                    'host' => $settings->host,
                    'port' => $settings->port,
                    'username' => $settings->username,
                    'password' => $settings->password,
                    'dbname' => $settings->dbname . '_queue',
                    'type' => 'pdo_mysql',
                    Zend_Queue::TIMEOUT => 1,
                    Zend_Queue::VISIBILITY_TIMEOUT => 1
                )
            );

            Mage::register($this->_name, new Zend_Queue('Db', $queueOptions));
        }
        return Mage::registry($this->_name);
    }

    /**
     * Run the CatalogRules from the Queue
     * @author Luis E.Tineo <luis@codegrease.com>
     * @return boolean
     */
    public function runCatalogRulesUpdate() {
        $this->log('Entering: ' . __FUNCTION__);
        $result = true;
        try {
            $ids = array();
            $this->_queue = $this->getQueue();
            $products = array();
            foreach ($this->_queue->receive(self::MAX_ALLOWED_MESSAGES) as $message) {
                $id = $message->md5;
                if (!in_array($id, $ids)) {
                    //don't process messages with the same id (this will happen if the product is saved multiple times)
                    $ids[] = $id;
                    $this->log('Processing Message ID: ' . $id);
                    $data = unserialize($message->body);
                    $this->log('We are working with ID: ' . $id);
                    $products [$data['pid']] = $data['handle'];
                }
                //delete the messages - don't need to keep messages if they are proccessed
                if ($this->_queue->deleteMessage($message)) {
                    $this->log("Message $id: was deleted");
                }
            }
            //do we have any products to work with?
            if (count($products)) {
                /**
                 * Get a collection
                 */
                $collection = Mage::getModel('catalog/product')->getCollection();
                $collection->addFieldToFilter('entity_id', array('in' => array_keys($products)));

                $this->log("Working with select: " . $collection->getSelect());

                foreach ($collection as $product) {
                    list ($class, $method) = explode("||", $products[$product->getId()]);
                    Mage::getModel($class)->$method($product);
                }
            } else {
                $this->log('Didnt get products to work wth');
            }
        } catch (Exception $e) {
            if (function_exists('getExceptionTraceAsString')) {
                $this->log("\n" . $e->getMessage() . getExceptionTraceAsString($e));
            } else {
                $this->log("\n" . $e->getMessage() . $this->_getExceptionTraceAsString($e));
            }
            $result = false;
        }

        $this->log('Exiting: ' . __FUNCTION__);
        return $result;
    }

    protected function _getExceptionTraceAsString($exception) {
        $rtn = "";
        $count = 0;
        foreach ($exception->getTrace() as $frame) {
            $args = "";
            if (isset($frame['args'])) {
                $args = array();
                foreach ($frame['args'] as $arg) {
                    if (is_string($arg)) {
                        $args[] = "'" . $arg . "'";
                    } elseif (is_array($arg)) {
                        $args[] = "Array";
                    } elseif (is_null($arg)) {
                        $args[] = 'NULL';
                    } elseif (is_bool($arg)) {
                        $args[] = ($arg) ? "true" : "false";
                    } elseif (is_object($arg)) {
                        $args[] = get_class($arg);
                    } elseif (is_resource($arg)) {
                        $args[] = get_resource_type($arg);
                    } else {
                        $args[] = $arg;
                    }
                }
                $args = join(", ", $args);
            }
            $rtn .= sprintf("#%s %s(%s): %s%s(%s)\n", $count, $frame['file'], $frame['line'], isset($frame['class']) ? $frame['class'] . '->' : '', $frame['function'], $args);
            $count++;
        }
        return $rtn;
    }

    /**
     * save the error message $error
     * @param string $error
     */
    final private function log($error) {
        if ($this->_debug) {
            Mage::log($error, Zend_Log::DEBUG, $this->_log);
        }
    }

}
