# CodeGrease_CatalogRule #

### Authors:
- [Luis Tineo](https://twitter.com/kingletas)

CodeGrease_CatalogRule basic Zend Queue implementation to change Magento's Catalog Rules update to an async way.

This module is useful when you have multiple products, customer groups, websites and catalog rules updates.
## Changelog

* 1.0.0: Initial release